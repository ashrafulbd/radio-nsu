Date	Day 	Name	Type
Jan 1	Tuesday	New Year's Day	Optional Holiday
Jan 10	Thursday	Bangabandhu Homecoming Day	Observance
Feb 10	Sunday	Saraswati Puja	Optional Holiday
Feb 14	Thursday	Valentine's Day	Observance
Feb 19	Tuesday	Maghi Purnima	Optional Holiday
Feb 21	Thursday	Language Martyrs' Day	Public Holiday
Mar 2	Saturday	National Flag Day	Observance
Mar 4	Monday	Maha Shivaratri	Optional Holiday
Mar 6	Wednesday	Ash Wednesday	Optional Holiday
Mar 17	Sunday	Sheikh Mujibur Rahman’s birthday	Public Holiday
Mar 21	Thursday	March Equinox	Season
Mar 21	Thursday	Holi	Hindu Holiday
Mar 21	Thursday	Doljatra	Optional Holiday
Mar 26	Tuesday	Independence Day	Public Holiday
Apr 3	Wednesday	Shab-e-Meraj	Optional Holiday
Apr 13	Saturday	Chaitra Sankranti	Optional Holiday
Apr 14	Sunday	Bengali New Year	Public Holiday
Apr 18	Thursday	Maundy Thursday	Optional Holiday
Apr 19	Friday	Good Friday	Optional Holiday
Apr 20	Saturday	Holy Saturday	Optional Holiday
Apr 21	Sunday	Easter Day	Optional Holiday
Apr 22	Monday	Shab e-Barat	Public Holiday
Apr 22	Monday	Easter Monday	Optional Holiday
May 1	Wednesday	May Day	Public Holiday
May 12	Sunday	Mothers' Day	Observance
May 18	Saturday	Buddha Purnima/Vesak	National holiday
May 31	Friday	Jumatul Bidah	Public Holiday
Jun 2	Sunday	Shab-e-qadr	Public Holiday
Jun 4	Tuesday	Eid ul-Fitr Eve Holiday	National holiday
Jun 5	Wednesday	Eid ul-Fitr	Public Holiday
Jun 6	Thursday	Eid ul-Fitr Holiday	Public Holiday
Jun 7	Friday	Eid ul-Fitr Holiday	Public Holiday
Jun 16	Sunday	Fathers' Day	Observance
Jun 21	Friday	June Solstice	Season
Jul 1	Monday	July 1 Bank Holiday	Bank Holiday
Jul 16	Tuesday	Ashari Purnima	Optional Holiday
Aug 12	Monday	Eid al-Adha	Public Holiday
Aug 13	Tuesday	Eid al-Adha Day 2	Public Holiday
Aug 14	Wednesday	Eid al-Adha Day 3	Public Holiday
Aug 15	Thursday	Raksha Bandhan	Hindu Holiday
Aug 15	Thursday	National Mourning Day	Public Holiday
Aug 15	Thursday	Eid al-Adha Day 4	Optional Holiday
Aug 23	Friday	Janmashtami	Public Holiday
Sep 2	Monday	Ganesh Chaturthi	Hindu Holiday
Sep 10	Tuesday	Ashura	Public Holiday
Sep 13	Friday	Madhu Purnima	Optional Holiday
Sep 23	Monday	September Equinox	Season
Sep 28	Saturday	Mahalaya	Optional Holiday
Sep 29	Sunday	First Day of Navaratri	Hindu Holiday
Oct 8	Tuesday	Durga Puja	Public Holiday
Oct 13	Sunday	Lakshmi Puja	Optional Holiday
Oct 13	Sunday	Prabarana Purnima	Optional Holiday
Oct 23	Wednesday	Akhari Chahar Somba	Optional Holiday
Oct 27	Sunday	Sri Shayama Puja	Optional Holiday
Oct 31	Thursday	Halloween	Observance
Nov 10	Sunday	Eid e-Milad-un Nabi	Public Holiday
Dec 9	Monday	Fateha-i-Yajdaham	Optional Holiday
Dec 16	Monday	Victory Day	Public Holiday
Dec 22	Sunday	December Solstice	Season
Dec 24	Tuesday	Christmas Eve	Optional Holiday
Dec 25	Wednesday	Christmas Day	Public Holiday
Dec 26	Thursday	Boxing Day	Optional Holiday
Dec 31	Tuesday	New Year's Eve	Bank Holiday