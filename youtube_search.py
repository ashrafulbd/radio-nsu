# Link Genarator from Youtube

import urllib.request
from bs4 import BeautifulSoup


def youtube_link_genarator( textToSearch ):
    # textToSearch = 'alo alo tumi kokhno by tahsan'
    query = urllib.parse.quote(textToSearch)
    url = "https://www.youtube.com/results?search_query=" + query
    response = urllib.request.urlopen(url)
    html = response.read()
    soup = BeautifulSoup(html, 'html.parser')
    for vid in soup.findAll(attrs={'class':'yt-uix-tile-link'}):
        # print('https://www.youtube.com' + vid['href'])
        return('https://www.youtube.com' + vid['href'])
        break





playlist_file = open("playlist.txt", "r")
playlist = playlist_file.readlines()

playlist_url_file = open("playlist_url.txt", "w")


for x in playlist:
    playlist_url_file.write(youtube_link_genarator(x) + '\n')
    # print(r)


